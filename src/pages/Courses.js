import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard';


export default function Courses() {

	
	// Checks if the mock data was captured.
	console.log(coursesData);
	console.log(coursesData[0]);

	// The "course" in the CourseCard component is called a 'Prop' which is a shorthand for "property" since components are considered as objects in React JS
	// The curly braces ({}) are used for props to signify that we are providing information using Javascript expressions
	// We can pass information from one component to another using props. This is referred to as 'props drilling'

	// The "map" method loops through the individual course objects in our array and returns a component for each courses

	const courses = coursesData.map(course => {
		return (
			<CourseCard key={course.id} course={course} />
		);
	})

	return(
		<>
		{courses}
		</>
	)

}