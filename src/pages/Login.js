import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';

import { useNavigate, Navigate } from 'react-router-dom';

export default function Login(props){

	// Allows us to consume the User Context object and it's properties to use for user validation
	const { user, setUser } = useContext(UserContext);

	// hook return a function that lets you navigate to components.
	const navigate = useNavigate();

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const [isActive, setIsActive] = useState("true")

	function authenticate(e) {

		e.preventDefault();

		// Set the email of the authenticated user in the local storage
			// Syntax
			// localStorage.setItem('propertyName', value)
		localStorage.setItem("email", email);

		// In this case, the key name is 'email' and the value is the value of the email variable. This code sets the value of 'email' key in the local storage to the value of the email variable.

		setUser( { email: localStorage.getItem('email')} );

		setEmail("");
		setPassword("");
		//navigate('/');

		alert(`"${email}" has been verified! Welcome back!`);
	}

	useEffect(() => {
		if(email !== "" && password !== ""){
			setIsActive(true)
		}else {
			setIsActive(false)
		}
	}, [email, password]);

	return (
		(user.email !== null) ? 
			<Navigate to="/courses" />
		:

		<Form onSubmit={(e) => authenticate(e)}>
			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email here"
					value={email}
					onChange={(e) => setEmail(e.target.value)}
					required
				/>
			</Form.Group>
			<Form.Group controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Password"
					value={password}
					onChange={(e) => setPassword(e.target.value)}
					required
				/>
			</Form.Group>
			{ isActive ? 

				<Button variant="primary my-3" type="submit" id="submitBtn">Submit</Button>

				: 

				<Button variant="danger my-3" type="submit" id="submitBtn" disabled>Submit</Button>

			}
			
		</Form>
	)
}